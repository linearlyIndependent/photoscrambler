{-# LANGUAGE OverloadedStrings #-}

import qualified Data.ByteString as B (getLine)
import qualified Data.ByteString.Lazy as BL hiding (putStrLn)
import qualified Data.ByteString.Char8 as CS (unpack) 
import qualified Data.ByteString.Lazy.Char8 as C (putStrLn, hPutStrLn, drop) 
import Data.Word (Word8)
import System.IO
import Data.List.Split
import System.Random


main = C.putStrLn "Enter the full path of the file you want to encrypt:" >>
       takeWhile (/= '\r') . CS.unpack <$> B.getLine >>= \path ->
       C.putStrLn "Enter the full path of the encrypted file:" >>
       takeWhile (/= '\r') . CS.unpack <$> B.getLine >>= \newPath ->
       fst . BL.foldr helper (BL.empty, mkStdGen 1337) <$> BL.readFile path >>= \output ->
       BL.writeFile newPath output
       -- Generates a Bool (determines whether the left or the right value will be the original).
       -- Generates a Word8 x, where originalValue =< x <= 255, which is the fake value
       where helper x (accStr, gen) = let (right,gen2) = randomR (True,False) gen
                                          (chr, gen3)  = randomR (x,255) gen2 
                                       in if right
                                            then (BL.cons chr $ BL.cons x accStr, gen3)
                                            else (BL.cons x $ BL.cons chr accStr, gen3)
                             
                                 