# PhotoScrambler

Two small sample programs for encrypting, decrypting files. The program is a solution for an excercise from bnd.bund.at . 
The encryption is not means secure. It uses the same seed and a generator which is not cryptographically secure.

Encrypting: <br/>
for each byte (x)<br/>
    generate a fake byte y with (x <= y <= 255)<br/>
    generate a boolean b<br/>
    if b then write the two bytes in the order y x<br/>
         else write the two bytes in the order x y<br/>
         
Decrypting: <br/>
for each byte-pair (x,y)<br/>
    z = min(x,y)<br/>
    write z<br/>
         