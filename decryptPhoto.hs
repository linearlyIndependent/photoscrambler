{-# LANGUAGE OverloadedStrings #-}

import qualified Data.ByteString as B (getLine)
import qualified Data.ByteString.Lazy as BL hiding (putStrLn)
import qualified Data.ByteString.Char8 as CS (unpack) 
import qualified Data.ByteString.Lazy.Char8 as C (putStrLn, hPutStrLn, drop) 
import Data.Word (Word8)
import System.IO
import Data.List.Split

main = C.putStrLn "Enter the full path of the file you want to decrypt:" >>
       takeWhile (/= '\r') . CS.unpack <$> B.getLine >>= \path ->
       C.putStrLn "Enter the full path of the decrypted file:" >>
       takeWhile (/= '\r') . CS.unpack <$> B.getLine >>= \newPath ->
       BL.pack . map helper . chunksOf 2 . BL.unpack <$> BL.readFile path >>= \output -> 
       BL.writeFile newPath output
        where helper [x] = x
              helper (x:xx:_) = min x xx
              
              
              
              
              
{-
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.ByteString as B (getLine)
import qualified Data.ByteString.Lazy as BL hiding (putStrLn)
import qualified Data.ByteString.Lazy.Char8 as C (putStrLn, hPutStrLn, drop) 
import Data.Word (Word8)
import System.IO
import Control.Monad
import Data.Maybe
import Data.List.Split

main = BL.writeFile "Urlaubsphoto.png" =<<(BL.pack . map helper . chunksOf 2 . BL.unpack <$> BL.readFile "Urlaubsphoto.png.crypt")
        where helper [x] = x
              helper (x:xx:_) = min x xx
              
              
-}